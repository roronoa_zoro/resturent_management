import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Category } from '../models/categoryModel';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

	private category:Category[];

  private categoriesUrl = 'api/categories';  // URL to web api

  constructor(private http: HttpClient) { }

  /** GET categories from the server */
  getAllCategory(): Observable<Category[]> {

    return this.http.get<Category[]>(this.categoriesUrl);
  }


  addCategory(category:Category):Observable<any>{

  	return this.http.post<Category>(this.categoriesUrl, category, httpOptions);
  }

  /** DELETE category from the server */
  deleteCat(category:Category):Observable<Category>{
    const url = `${this.categoriesUrl}/${category.id}`;

    return this.http.delete<Category>(url, httpOptions);
  }

  /** GET category from the server */
  getCategory(id:number):Observable<Category>{
    const url = `${this.categoriesUrl}/${id}`;
    return this.http.get<Category>(url);
  }

  /** Update category from the server */
  updateCategory(category:Category):Observable<Category>{
    return this.http.post<Category>(this.categoriesUrl, category, httpOptions);
  }
}
