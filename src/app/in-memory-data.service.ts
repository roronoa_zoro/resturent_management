import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Category } from './models/categoryModel';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  constructor() { }

  createDb(){
  	const categories = [
  			  { id: 11, name: 'Dr Nice' },
		      { id: 12, name: 'Narco' },
		      { id: 13, name: 'Bombasto' },
		      { id: 14, name: 'Celeritas' },
		      { id: 15, name: 'Magneta' },
		      { id: 16, name: 'RubberMan' },
		      { id: 17, name: 'Dynama' },
		      { id: 18, name: 'Dr IQ' },
		      { id: 19, name: 'Magma' },
		      { id: 20, name: 'Tornado' }
  			];

  			return {categories};
	}

	genCatId(categories:Category[]):number{
		return categories.length > 0 ? Math.max(...categories.map(cat => cat.id)) + 1 : 11;
	}
}
