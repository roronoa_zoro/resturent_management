import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './components/category/category.component';
import { CategoryDetailComponent } from './components/category/category-detail/category-detail.component';


const routes: Routes = [
  //{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '', component: CategoryComponent },
  { path: 'categories', component: CategoryComponent },
  { path: 'detail/:id', component: CategoryDetailComponent },
  //{ path: 'heroes', component: HeroesComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}