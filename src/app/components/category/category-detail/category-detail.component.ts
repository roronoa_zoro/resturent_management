import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Category } from '../../../models/categoryModel';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {
	@Input() category:Category;
  constructor( 	private route: ActivatedRoute,
  				private catService: CategoryService,
  				private location: Location) { }

  ngOnInit(): void {
  	this.getCategory();
  }

	getCategory():void{
		const id = +this.route.snapshot.paramMap.get('id');
		this.catService.getCategory(id).subscribe(cat =>{
			this.category = cat;
		});
	}

	 goBack(): void {
	    this.location.back();
	  }

	onSubmit(){
		this.catService.updateCategory(this.category).subscribe(() => this.goBack());
	}

}
