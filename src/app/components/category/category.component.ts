import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/categoryModel';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
	categories:Category[];

  constructor(private catService: CategoryService) { }

  ngOnInit() {
  
    this.catService.getAllCategory().subscribe(categories => {
          this.categories = categories;
    });
  }

 deleteCategory(category:Category){
      // delete from UI
  	  this.categories = this.categories.filter(c => category.name != c.name);

      // delete from server
      this.catService.deleteCat(category).subscribe(() =>{
        console.log('success');
      });
  }

  addCategory(category:Category){
    this.catService.addCategory(category).subscribe(cat => {
      this.categories.push(cat);
    });
  }

}
