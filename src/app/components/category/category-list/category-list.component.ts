import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../../../models/categoryModel';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
	@Input() category:Category[];
	@Output() deleteTodo:EventEmitter<Category> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onDelete(category){
  	this.deleteTodo.emit(category);
  }

}
