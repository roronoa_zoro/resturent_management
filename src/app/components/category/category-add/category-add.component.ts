import { Component, OnInit, Output, EventEmitter } from '@angular/core';

//import { Category } from '../../../models/categoryModel';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {
	
	@Output() addCategory:EventEmitter<any> = new EventEmitter();
	name:string;

  constructor() { }

  ngOnInit() {
  }

  onSubmit(){

  	//console.log(category);
  	const category = {
  		name: this.name
  	}
  	this.addCategory.emit(category);
  }

  /* get category for update*/


}
