export class Category {
	public id:number;
	public name:string;

	constructor(id:number, name:string) {
		// code...
		this.id = id;
		this.name = name;
	}
}